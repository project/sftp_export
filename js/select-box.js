(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.MyModuleBehavior = {
    attach: function (context, settings) {
      var pathname = window.location.pathname;

      $(".form-checkbox").on("click", function () {
        if (pathname == "/admin/config/services/sftp_settings/content_config") {
          if ($(this).is(":checked") == false) {
            var className = $(this).attr("class").split(" ")[0];
            if (className.indexOf("check-box") != -1) {
              className = className.replace("check-box", "field-set");
              $("select." + className).attr("disabled", "true");
              $("select." + className).css("color", "grey");
            }
          } else {
            var className = $(this).attr("class").split(" ")[0];
            if (className.indexOf("check-box") != -1) {
              className = className.replace("check-box", "field-set");
              $("select." + className).prop('disabled',false)
              $("select." + className).removeAttr("style");
            }
          }
        }
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
