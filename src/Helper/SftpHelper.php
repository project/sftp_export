<?php

namespace Drupal\sftp_data_export\Helper;

/**
 * Packages sftp helper for support functions.
 */
class SftpHelper {

  const FOLDER_NAME = 'sftp';
  const ROOT_DIRECTORY = 'public';

  /**
   * Return the node fields.
   */
  public function getNodeFields($node, $fields) {
    $data = [];
    foreach ($fields as $field) {
      $field_type = $node->hasField($field) ? $node->get($field)->getFieldDefinition()->getType() : NULL;

      switch ($field_type) {
        case 'image':
          $data[] = $node->hasField($field) ? $node->get($field)->entity->uri->value : NULL;
          break;

        case 'list_float':
          $value = $node->hasField($field) ? $node->get($field)->getValue()[0]['value'] : NULL;
          $allowed_values = $node->$field->getSetting('allowed_values');
          $data[] = $allowed_values[$value];
          break;

        case 'list_integer':
          $value = $node->hasField($field) ? $node->get($field)->getValue()[0]['value'] : NULL;
          $allowed_values = $node->$field->getSetting('allowed_values');
          $data[] = $allowed_values[$value];
          break;

        case 'list_string':
          $value = $node->hasField($field) ? $node->get($field)->getValue()[0]['value'] : NULL;
          $allowed_values = $node->$field->getSetting('allowed_values');
          $data[] = $allowed_values[$value];
          break;

        case 'created':
        case 'timestamp':
          $value = $node->hasField($field) ? $node->get($field)->getValue()[0]['value'] : NULL;
          $value = date('m/d/Y H:i:s', $value);
          $data[] = $value;
          break;

        case 'link':
          $value = $node->get($field)->getValue()[0]['uri'];
          $data[] = $value;
          break;

        default:
          $data[] = $node->hasField($field) && $node->get($field)->getValue() ? $node->get($field)->getValue()[0]['value'] : NULL;
          break;
      }
    }
    return $data;
  }

  /**
   * Function to get only base fields.
   */
  public function getCleanFields($fields) {
    $removeFields = [
      'entity_reference',
      'entity_reference_revision',
      'entity_reference_revisions',
      'map',
      'changed',
      'metatag',
      'uuid',
      'comments',
    ];
    $cleanFields = [];
    foreach ($fields as $field => $value) {
      if (!in_array($value->getType(), $removeFields)) {
        $cleanFields[$field] = $value;
      }
    }
    return $cleanFields;
  }

}
