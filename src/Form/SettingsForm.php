<?php

namespace Drupal\sftp_data_export\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sftp_data_export\Helper\SftpHelper;

/**
 * Configure SFTP Data Export settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sftp_data_export_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sftp_data_export.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $sftpHelper = new SftpHelper();
    $content_types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $form['content_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Content Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['content_tab']['settings_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Content type'),
        $this->t('Fields'),
      ],
    ];
    foreach ($content_types as $content_type) {
      // Get fields of content type.
      $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $content_type->id());
      // $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', '4k_tv');
      $clean_fields = $sftpHelper->getCleanFields($fields);
      $fieldset = [];
      foreach ($clean_fields as $field) {
        $fieldset[$field->getName()] = $field->getName();
      }
      $field_flag = FALSE;
      if (empty($this->config('sftp_data_export.settings')->getRawData()[$content_type->id()]['flag'])) {
        $field_flag = TRUE;
      }

      // Content type list.
      $class = strtolower(strtolower($content_type->label()));
      $class = str_replace(" ", "-", $class);

      $default_flag = $this->config('sftp_data_export.settings')->getRawData()[$content_type->id()]['flag'];
      $default_fields = $this->config('sftp_data_export.settings')->getRawData()[$content_type->id()]['fields'];
      $form['content_tab']['settings_table'][$content_type->id()]['content_type'] = [
        '#type' => 'checkbox',
        '#title' => $content_type->label(),
        '#attributes' => ['class' => [$class . '-check-box']],
        '#default_value' => $default_flag,
      ];
      // Field list of content type.
      $form['content_tab']['settings_table'][$content_type->id()]['fields'] = [
        '#type' => 'select',
        '#options' => $fieldset,
        '#attributes' => ['class' => [$class . '-field-set']],
        '#disabled' => $field_flag,
        '#default_value' => $default_fields,
        '#multiple' => TRUE,
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues()['settings_table'] as $key => $value) {
      if ($value['content_type'] == 1 && empty($value['fields'])) {
        $form_state->setErrorByName('sftp_data_export', $this->t('Content type <b>@content_type</b> is selected without any fields will not export any data.', ['@content_type' => $key]));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configuration for each flag and fields.
    foreach ($form_state->getValues()['settings_table'] as $key => $value) {
      if ($value['content_type'] == 1) {
        $data['flag'] = $value['content_type'];
        $data['fields'] = $value['fields'];
        $this->config('sftp_data_export.settings')
          ->set($key, $data)
          ->save();
      }
    }
    parent::submitForm($form, $form_state);
  }

}
