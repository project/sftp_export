<?php

namespace Drupal\sftp_data_export\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sftp_data_export\Helper\SftpHelper;

/**
 * Packages UploadForm to updoad assets to remote server.
 */
class UploadForm extends FormBase {

  /**
   * Configfactory interface.
   *
   * @var object
   */
  protected $configFactory;

  /**
   * Settings object.
   *
   * @var object
   */
  protected $settings;

  /**
   * S3FS configuration settings.
   *
   * @var object
   */
  protected $s3fs;

  /**
   * Get current folder.
   *
   * @var string
   */
  protected $folder;

  /**
   * Get common helper object.
   *
   * @var object
   */
  protected $commonHelper;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $contentcofig = \Drupal::config('sftp_data_export.settings')->getRawData();
    $options['none'] = 'None';
    if (!empty($contentcofig)) {
      foreach ($contentcofig as $key => $value) {
        $content_type = \Drupal::entityTypeManager()->getStorage('node_type')->load($key);
        $options[$key] = $content_type->label();
      }
    }
    $form['export'] = [
      '#type' => 'details',
      '#title' => $this->t('Export'),
      '#collapsible' => TRUE,
      '#open' => TRUE,
    ];
    $form['export']['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#description' => $this->t('All nodes of the above bundle will be exported in the csv format on sftp server.'),
      '#options' => $options,
    ];
    $form['export']['button'] = [
      '#type' => 'submit',
      '#value' => 'Export',
      '#submit' => [[$this, 'batchExportCsv']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Function to write csv and export file to sftp server.
   */
  public function batchExportCsv(array &$form, FormStateInterface $form_state) {
    $sftpHelper = new SftpHelper();
    $database = \Drupal::database();
    $rootFolder = $sftpHelper::ROOT_DIRECTORY;
    $sftpfolder = $sftpHelper::FOLDER_NAME;
    $bundle = $form_state->getValue('bundle');
    $contentcofig = \Drupal::config('sftp_data_export.settings')->get($bundle);
    $data = $database->select('node', 'n')
      ->condition('n.type', $bundle, '=')
      ->fields('n', ['nid'])
      ->execute()->fetchAll();
    $headers[] = $contentcofig['fields'];
    // Row Set.
    $row_count = count($data);
    $row_chunk = range(1, $row_count);
    $row_chunk = array_chunk($row_chunk, 10);
    // Nids set.
    $nids_chunk = array_chunk($data, 10);

    $file = $rootFolder . '://' . $sftpfolder . '/' . $bundle . '_' . date("d-m-Y") . '.csv';
    $fp = fopen($file, 'w');
    foreach ($headers as $header) {
      fputcsv($fp, $header);
    }
    fclose($fp);
    foreach ($nids_chunk as $nid_chunk) {
      $operations[] = [
        '\Drupal\sftp_data_export\Batch\ExportBatch::exportCsvCallback',
        [
          json_decode(json_encode($nid_chunk), TRUE),
          $bundle,
        ],
      ];
    }
    $batch = [
      'operations' => $operations,
      'finished' => '\Drupal\sftp_data_export\Batch\ExportBatch::exportBatchFinishedCallback',
      'title' => $this->t('Writing data to csv file'),
      'init_message' => $this->t('Data export starting...'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('CSV batch encountered some error.'),
    ];
    batch_set($batch);
  }

}
