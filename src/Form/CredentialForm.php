<?php

namespace Drupal\sftp_data_export\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure SFTP credentials.
 */
class CredentialForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sftp_credentials';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sftp_data_export.cred'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['credentials'] = [
      '#type' => 'details',
      '#title' => $this->t('Credentials'),
      '#collapsible' => TRUE,
      '#open' => TRUE,
    ];
    $form['credentials']['user'] = [
      '#type' => 'textfield',
      '#title' => 'Username',
      '#default_value' => $this->config('sftp_data_export.cred')->get('username'),
      '#description' => $this->t('Username of the sftp server'),
    ];
    $form['credentials']['pass'] = [
      '#type' => 'password',
      '#title' => 'Password',
      '#default_value' => $this->config('sftp_data_export.cred')->get('password'),
      '#description' => $this->t('Password of the sftp server'),
    ];
    $form['others'] = [
      '#type' => 'details',
      '#title' => $this->t('Network config'),
      '#collapsible' => TRUE,
      '#open' => TRUE
    ];
    $form['others']['host'] = [
      '#type' => 'textfield',
      '#title' => 'Host',
      '#default_value' => $this->config('sftp_data_export.cred')->get('host'),
      '#description' => $this->t('IP address of the sftp server'),
    ];
    $form['others']['port'] = [
      '#type' => 'password',
      '#title' => 'Port',
      '#default_value' => $this->config('sftp_data_export.cred')->get('port'),
      '#description' => $this->t('Port of the sftp server'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('user')) || empty($form_state->getValue('pass')) || empty($form_state->getValue('host')) || empty($form_state->getValue('port'))) {
      $form_state->setErrorByName('sftp_data_export', $this->t('All of the fields are required in order to verify your remote server.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configuration for each flag and fields.
    $this->config('sftp_data_export.cred')
      ->set('username', $form_state->getValue('user'))
      ->set('password', $form_state->getValue('pass'))
      ->set('host', $form_state->getValue('host'))
      ->set('port', $form_state->getValue('port'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
