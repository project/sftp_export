<?php

namespace Drupal\sftp_data_export\Batch;

use Drupal\sftp_data_export\Helper\SftpHelper;

/**
 * Package ExportBatch callbacks for batch process.
 */
class ExportBatch {

  /**
   * Function to write csv and export it to sftp.
   */
  public function exportCsvCallback($nids_chunk, $bundle, &$context) {
    $sftphelper = new SftpHelper();
    $rootFolder = $sftphelper::ROOT_DIRECTORY;
    $sftpfolder = $sftphelper::FOLDER_NAME;
    $sftpConfig = \Drupal::config('sftp_data_export.settings')->get($bundle);
    $count = 0;
    $result = [];
    foreach ($nids_chunk as $nid) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid['nid']);
      $result[] = $sftphelper->getNodeFields($node, $sftpConfig['fields']);
      $count++;
    }
    $context['results']['count'][] = $count;
    // Path to save the CSV file.
    $file = $rootFolder . '://' . $sftpfolder . '/' . $bundle . '_' . date("d-m-Y") . '.csv';
    $context['results']['file_path'] = $file;
    $fp = fopen($file, 'a');
    foreach ($result as $fields) {
      fputcsv($fp, $fields);
    }
    fclose($fp);
  }

  /**
   * Export batch finished Callback.
   */
  public function exportBatchFinishedCallback($success, $results, $bundle, $operations) {
    $_FILES = 'Files';
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    $total_count = array_sum($results['count']);
    $file_name = $total_count . '_' . date("d-m-Y");
    $sftphelper = new SftpHelper();
    $configSftp = \Drupal::config('sftp_data_export.cred');
    if ($success) {
      $host = $configSftp->get('host');
      $port = $configSftp->get('port');
      $username = $configSftp->get('username');
      $pass = $configSftp->get('password');
      $resConnection = ssh2_connect($host, $port);
      if (ssh2_auth_password($resConnection, $username, $pass)) {
        \Drupal::logger('recon_refund')->notice("connected");
        $resSFTP = ssh2_sftp($resConnection);
        $desFile = fopen("ssh2.sftp://{$resSFTP}/" . $_FILES . '/' . $file_name . '.csv', 'w');
        $srcFile = fopen($results['file_path'], 'r');
        $writtenBytes = stream_copy_to_stream($srcFile, $desFile);
        fclose($desFile);
        fclose($srcFile);
        \Drupal::logger('recon_refund')->notice("Succesfully Uploaded");
      }
      else {
        \Drupal::logger('recon_refund')->notice("Not connected");
      }
      $message = t('@count items successfully processed', ['@count' => array_sum($results['count'])]);
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

}
